class_name Ramp extends Node2D

var speed: float
@onready var sprite: Sprite2D = $Top/Sprite2D

func size() -> Vector2:
	return sprite.texture.get_size() * scale

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	global_position.x -= speed * delta
		

func _on_top_body_entered(body: Node2D) -> void:
	var flappy = body as Flappy
	flappy.die()

func _on_bot_body_entered(body: Node2D) -> void:
	var flappy = body as Flappy
	flappy.die()
