extends Node2D

var last_ramp: Ramp
var score_ramp: Ramp
var speed: float = 150.0
var distance: float = 450.0
var previous_score = 0 # used for speed increase in the game

const SPEED_INCREASE = 40
const SCORE_THRESHOLD = 10
const RAMP_POSITION_FROM_EDGE = 200
const CORN_POSITION_FROM_EDGE = 400
const RAMPS_SPEED_ON_DASH = 1000

var is_dashing: bool = false

@onready var ramp_scene = preload("res://ramp.tscn")
@onready var corn_scene = preload("res://corn.tscn")
@onready var bird = $Flappy
@onready var hud = $Hud
@onready var background_music = $BackgroundMusic
@onready var camera = $Camera2D
@onready var dash_timer = $DashTimer
@onready var corn_spawn_timer = $CornSpawnTimer

var scale_factor

func generate_ramp() -> void:
	var ramp: Ramp = ramp_scene.instantiate()
	if last_ramp != null:
		ramp.global_position = Vector2(
			last_ramp.global_position.x + distance,
			randi_range(
				RAMP_POSITION_FROM_EDGE, 
				get_viewport_rect().size.y - RAMP_POSITION_FROM_EDGE
			)
		)
	else:
		ramp.global_position = Vector2(
			get_window().size.x / 2,
			randi_range(
				RAMP_POSITION_FROM_EDGE, 
				get_viewport_rect().size.y - RAMP_POSITION_FROM_EDGE
			)
		)
	
	if is_dashing:
		ramp.speed = speed + RAMPS_SPEED_ON_DASH
	else:
		ramp.speed = speed
	last_ramp = ramp
	add_child(ramp)
	
func spawn_corn() -> void:
	var corn = corn_scene.instantiate()
	corn.global_position = Vector2(
		last_ramp.global_position.x + distance/2,
		randi_range(
			CORN_POSITION_FROM_EDGE,
			get_viewport_rect().size.y - CORN_POSITION_FROM_EDGE
		)
	)
	if is_dashing:
		corn.speed = speed + RAMPS_SPEED_ON_DASH
	else:
		corn.speed = speed
	add_child(corn)

func init_array() -> void:
	for i in range(10):
		generate_ramp()

func _on_dash_clicked() -> void:
	speed_up_the_game(RAMPS_SPEED_ON_DASH)
	is_dashing = true
	dash_timer.start()

func _ready() -> void:
	init_array()
	bird.connect("dead", hud._on_game_over)
	bird.connect("dash_clicked", _on_dash_clicked)
	bird.connect("number_of_dashes_changed", _on_number_of_dashes_changed)
	hud.connect("restart_game", _restart_game)

func _on_number_of_dashes_changed(value):
	hud.number_of_dashes = value

func _process(delta: float) -> void:    
	for child in get_children():
		if child is Ramp:
			if (child.global_position.x + child.size().x) < 0:
				generate_ramp()
				child.free()
				continue
				
			elif bird.global_position.x >= child.global_position.x + \
				child.size().x && child != score_ramp && !bird.dying:
					hud.score += 1
					score_ramp = child
		if child is Corn:
			if (child.global_position.x + child.size().x) < 0:
				child.queue_free()
	
	if hud.score - previous_score >= SCORE_THRESHOLD:
		speed_up_the_game(SPEED_INCREASE)
		speed += SPEED_INCREASE
		previous_score = hud.score
		
	
func speed_up_the_game(amount: int) -> void:
	for child in get_children():
		if child is Ramp or child is Corn:
			child.speed += amount

func _restart_game() -> void:
	get_tree().reload_current_scene()

func _on_background_music_finished() -> void:
	background_music.play()


func _on_dash_timer_timeout() -> void:
	speed_up_the_game(-RAMPS_SPEED_ON_DASH)
	is_dashing = false


func _on_corn_spawn_timer_timeout() -> void:
	spawn_corn()
	corn_spawn_timer.start(randi_range(5, 10))
