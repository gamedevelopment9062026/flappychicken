extends ParallaxBackground

func _physics_process(delta: float) -> void:
	# Update the scroll offset manually
	scroll_offset.x -= 50.0 * delta
