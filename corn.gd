class_name Corn extends Area2D

var speed: float

func size() -> Vector2:
	return $Sprite2D.texture.get_size() * scale

func _process(delta: float) -> void:
	global_position.x -= speed * delta

func _on_body_entered(body: Node2D) -> void:
	if body is Flappy:
		body.add_dash()
		queue_free()
