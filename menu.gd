class_name Menu extends CanvasLayer

@onready var music = $AudioStreamPlayer2D

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("jump"):
		get_tree().change_scene_to_file("res://game.tscn")
	elif Input.is_action_just_pressed("exit"):
		get_tree().quit()

func _ready() -> void:
	music.play()

func _on_audio_stream_player_2d_finished() -> void:
	music.play()

