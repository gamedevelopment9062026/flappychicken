class_name Flappy extends CharacterBody2D

signal dead

const JUMP_VELOCITY = -360.0

@onready var animation: AnimatedSprite2D = $AnimatedSprite2D
@onready var jump_sound = $JumpSound
@onready var hit_sound = $HitSound
@onready var dash_sound = $DashSound

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: int =   ProjectSettings.get_setting("physics/2d/default_gravity")

var killed: bool = false
var dying: bool = false

signal number_of_dashes_changed(current_number)

var number_of_dashes: int = 3

const MAX_NUMBER_OF_DASHES = 3

signal dash_clicked

func die() -> void:
	killed = true
	if !dying:
		hit_sound.play()
		
func add_dash() -> void:
	number_of_dashes += 1
	number_of_dashes = clamp(number_of_dashes, 0, MAX_NUMBER_OF_DASHES)
	number_of_dashes_changed.emit(number_of_dashes)

func _physics_process(delta: float) -> void:
	
	velocity.y += gravity * delta
	
	if !killed && !dying:
		if Input.is_action_just_pressed("jump"):
			velocity.y = JUMP_VELOCITY
			jump_sound.play()
			
		if Input.is_action_just_pressed("dash"):
			if number_of_dashes > 0:
				dash_clicked.emit()
				dash_sound.play()
				number_of_dashes -= 1
				number_of_dashes_changed.emit(number_of_dashes)
		
		if velocity.y < 0:
			animation.play("up")
		else:
			animation.play("down")
	elif killed && !dying:
		animation.flip_v = true
		animation.play("down")
		velocity.y = -200
		killed = false
		dying = true
		
	if global_position.y > get_window().size.y:
		dead.emit()
		dying = true
	
	move_and_slide()
	
	
