class_name Hud extends CanvasLayer

@onready var score_label = $Control/MarginContainer/Score
@onready var game_over_label = $Control/MarginContainer/VBoxContainer/GameOver
@onready var press_label = $Control/MarginContainer/VBoxContainer/PressToRestart
@onready var dash_container = $Control/MarginContainer2/DashContainer
var dash_icon: Resource = preload("res://Assets/dash_icon.png")
var dash_sprite: Sprite2D

enum Input_Type { KEYBOARD, GAMEPAD }

var last_input_type: Input_Type = Input_Type.KEYBOARD

var game_over: bool = false

var number_of_dashes: int = 3:
	set(value):
		number_of_dashes = value
		_make_dashes_invisible()
		_update_dashes()

func _make_dashes_invisible() -> void:
	for child in dash_container.get_children():
		var trect = child as TextureRect
		trect.visible = false

func _update_dashes() -> void:
	for i in range(number_of_dashes):
		var trect = dash_container.get_child(i) as TextureRect
		trect.visible = true
		

signal restart_game

var score: int = 0:
	set(value):
		score = value
		_update_score_label()
	get:
		return score
		

func _ready() -> void:
	_update_score_label()

func _input(event: InputEvent) -> void:
	if event is InputEventJoypadButton:
		last_input_type = Input_Type.GAMEPAD
	else:
		last_input_type = Input_Type.KEYBOARD

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("jump"):
		if game_over:
			restart_game.emit()
	elif Input.is_action_just_pressed("exit"):
		if game_over:
			get_tree().quit()

func _update_score_label() -> void:
	score_label.text = str(score)
	
func _on_game_over() -> void:
	game_over_label.text = "Game Over"
	if last_input_type == Input_Type.KEYBOARD:
		press_label.text = "(press SPACE to continue or ESC to quit)"
	else:
		press_label.text = "(press A to continue or B to quit)"
	game_over = true
